import { shoeArr } from "../../data_shoeShop";
import { ADD_TO_CART } from "../constant/shoeShopConstant";

let intialState = {
  shoeArr: shoeArr,
  gioHang: [shoeArr[0]],
};

export let shoeShopeReducer = (state = intialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let index = state.gioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      let cloneGioHang = [...state.gioHang];
      if (index == -1) {
        let newSp = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSp);
        // th1
      } else {
        cloneGioHang[index].soLuong++;
        // th2
      }

      state.gioHang = cloneGioHang;
      return { ...state };
    }
    default:
      return state;
  }
};
