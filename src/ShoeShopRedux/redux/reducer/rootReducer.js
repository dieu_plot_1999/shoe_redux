import { combineReducers } from "redux";
import { shoeShopeReducer } from "./shoeShopReducer";

export let rootReducer_shoeShop = combineReducers({
  shoeShopeReducer,
});
