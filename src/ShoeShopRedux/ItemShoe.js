import React, {Component} from "react";
import {connect} from "react-redux";
import {ADD_TO_CART} from "./redux/constant/shoeShopConstant";
class ItemShoe extends Component {
  render() {
    let {image, name, description} = this.props.shoeData;
    return (
      <div className="col-3">
        <div className="card" style={{width: "100%"}}>
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">
              {description.length < 30
                ? description
                : description.slice(0, 30) + "..."}
            </p>
            <button
              onClick={() => {
                this.props.handleOnclick(this.props.shoeData);
              }}
              className="btn btn-primary"
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleOnclick: (sp) => {
      dispatch({
        type: ADD_TO_CART,
        payload: sp,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
