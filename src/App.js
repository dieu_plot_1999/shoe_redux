import "./App.css";
import ShoeShopRedux from "./ShoeShopRedux/ShoeShopRedux";
function App() {
  return (
    <div className="App">
      <ShoeShopRedux />
    </div>
  );
}

export default App;

// npm i redux react-redux
// redux : state management library

// react-redux : thư viện kết nối react và redux
